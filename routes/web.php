<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/sales/chart', 'SaleController@chart');

Route::get('/home', 'SaleController@chartDrilldown');

Auth::routes(['verify' => true]);
//teste
//Route::get('/home', 'HomeController@index')->middleware('verified');

Route::resource('sales', 'SaleController');