




<li class="nav-item {{ Request::is('sales*') ? 'active' : '' }}">
    <a class="nav-link" href="home">
        <i class="nav-icon icon-cursor"></i>
        <span>Home</span>
    </a>
    <a class="nav-link" href="{{ route('sales.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sales</span>
    </a>
</li>
