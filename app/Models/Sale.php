<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Sale
 * @package App\Models
 * @version August 31, 2020, 11:55 am UTC
 *
 * @property string $seller
 * @property number $price
 * @property string $date
 */
class Sale extends Model
{

    public $table = 'sales';
    



    public $fillable = [
        'seller',
        'price',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'seller' => 'string',
        'price' => 'float',
        'date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'seller' => 'required',
        'price' => 'required',
        'date' => 'required'
    ];

    
}
