<?php

namespace App\Http\Controllers;

use App\DataTables\SaleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSaleRequest;
use App\Http\Requests\UpdateSaleRequest;
use App\Repositories\SaleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\Sale;


class SaleController extends AppBaseController
{
    /** @var  SaleRepository */
    private $saleRepository;

    public function __construct(SaleRepository $saleRepo)
    {
        $this->saleRepository = $saleRepo;
    }

    /**
     * Display a listing of the Sale.
     *
     * @param SaleDataTable $saleDataTable
     * @return Response
     */
    public function index(SaleDataTable $saleDataTable)
    {
        return $saleDataTable->render('sales.index');
    }

    /**
     * Show the form for creating a new Sale.
     *
     * @return Response
     */
    public function create()
    {
        return view('sales.create');
    }

    /**
     * Store a newly created Sale in storage.
     *
     * @param CreateSaleRequest $request
     *
     * @return Response
     */
    public function store(CreateSaleRequest $request)
    {
        $input = $request->all();

        $sale = $this->saleRepository->create($input);

        Flash::success('Sale saved successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Display the specified Sale.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        return view('sales.show')->with('sale', $sale);
    }

    /**
     * Show the form for editing the specified Sale.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        return view('sales.edit')->with('sale', $sale);
    }

    /**
     * Update the specified Sale in storage.
     *
     * @param  int              $id
     * @param UpdateSaleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaleRequest $request)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $sale = $this->saleRepository->update($request->all(), $id);

        Flash::success('Sale updated successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Remove the specified Sale from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $this->saleRepository->delete($id);

        Flash::success('Sale deleted successfully.');

        return redirect(route('sales.index'));
    }
    public function chartDrilldown(){

        $date = date('2019-01-01');
        $end_date = date('2020-09-01');
        $i =0;
        $m =0;
        $y = 0;
        
        $salesPriceByYear[0][1] = 0;
        $salesPriceByYear[0][0] = date("Y",strtotime($date));
        while (strtotime($date) <= strtotime($end_date)) {
            $from = $date;
            $to = date ("Y-m-d", strtotime("+1 month", strtotime($from)));
            
            
            
            $salesPriceByMonth[$y][$m][0] = date("M",strtotime($date));
            $salesPriceByMonth[$y][$m][1] = 0;
            
            
            
            $sales[$i] = Sale::whereBetween('date', [$from, $to])->get();

            for($j = 0; $j < sizeof($sales[$i]);$j++){
                $salesPriceByMonth[$y][$m][1] = $salesPriceByMonth[$y][$m][1] + $sales[$i][$j]['price']; 
            }
            

            $salesPriceByYear[$y][1] = $salesPriceByYear[$y][1]+ $salesPriceByMonth[$y][$m][1];
            


            $m++;
            if($m == 12){
                $m =0;

                $y = $y +1;
                $salesPriceByYear[$y][1] =0;
                $salesPriceByYear[$y][0] =date("Y",strtotime($to));


            }
            $i++;
            $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
        }

        return view('sales.chartDrilldown', compact('salesPriceByYear', 'salesPriceByMonth'));
    }
}
